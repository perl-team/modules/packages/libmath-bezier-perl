Source: libmath-bezier-perl
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Standards-Version: 3.9.6
Homepage: https://metacpan.org/release/Math-Bezier
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libmath-bezier-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libmath-bezier-perl.git

Package: libmath-bezier-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}
Multi-Arch: foreign
Description: Perl module for the solution of Bezier Curves
 Math::Bezier implements the algorithm for the solution of Bezier curves as
 presented by Robert D. Miller in Graphics Gems V, "Quick and Simple Bezier
 Curve Drawing".
 .
 A new Bezier curve is created using the new() constructor, passing a list of
 (x, y) control points.
 .
  use Math::Bezier;
  my @control = ( 0, 0, 10, 20, 30, -20, 40, 0 );
  my $bezier = Math::Bezier->new(@control);
 .
 Alternately, a reference to a list of control points may be passed.
